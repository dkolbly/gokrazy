package main

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/dkolbly/cli"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/font/gofont/goregular"
	"golang.org/x/image/math/fixed"
)

func main() {

	app := &cli.App{
		Name:  "clock",
		Usage: "supply a clock to a pitft display",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:  "center",
				Usage: "center time in image",
			},
			&cli.IntFlag{
				Name:    "size",
				Aliases: []string{"s"},
				Usage:   "font size",
				Value:   20,
			},
			&cli.StringFlag{
				Name:    "format",
				Aliases: []string{"f"},
				Usage:   "date format",
				Value:   "15:04",
			},
			&cli.StringFlag{
				Name:    "geom",
				Aliases: []string{"g"},
				Usage:   "WxH+X+Y",
				Value:   "100x20+0+0",
			},
			&cli.StringFlag{
				Name:  "server",
				Usage: "display server",
				Value: "http://pi3:8002",
			},
		},
		Action: doClock,
	}

	app.Run(os.Args)
}

var fullGeom = regexp.MustCompile(`^(\d+)x(\d+)([+-]\d+)([+-]\d+)$`)
var sizeOnly = regexp.MustCompile(`^(\d+)x(\d+)$`)

func parseGeom(s string) image.Rectangle {
	atoi := func(s string) int {
		n, _ := strconv.ParseInt(s, 10, 31)
		return int(n)
	}

	m := fullGeom.FindStringSubmatch(s)
	if m != nil {
		w := atoi(m[1])
		h := atoi(m[2])
		x := atoi(m[3])
		y := atoi(m[4])
		return image.Rectangle{
			Min: image.Point{
				X: x,
				Y: y,
			},
			Max: image.Point{
				X: x + w,
				Y: y + h,
			},
		}
	}
	m = sizeOnly.FindStringSubmatch(s)
	if m != nil {
		w := atoi(m[1])
		h := atoi(m[2])
		return image.Rectangle{
			Max: image.Point{
				X: w,
				Y: h,
			},
		}
	}
	return image.Rectangle{
		Max: image.Point{
			X: 100,
			Y: 20,
		},
	}
}

func doClock(c *cli.Context) error {

	r := parseGeom(c.String("geom"))
	fmt.Printf("Clock geometry: %s\n", r)

	usefont, err := truetype.Parse(goregular.TTF)
	if err != nil {
		panic(err)
	}

	size := c.Int("size")
	face := truetype.NewFace(
		usefont,
		&truetype.Options{
			Size: float64(size),
		})

	timeFormat := c.String("format")
	server := c.String("server")

	subsecond := false
	if strings.IndexByte(timeFormat, '.') >= 0 {
		subsecond = true
	}
	var posn Positioner
	posn = simplePosition{r}
	if c.Bool("center") {
		posn = &centerPosition{
			face: face,
			r:    r,
		}
	}

	var prev string
	for {
		t := time.Now()
		cur := t.Format(timeFormat)
		if cur != prev {
			fmt.Printf("%s : updating %q\n",
				t.Format(time.RFC3339Nano),
				cur)
			img := renderClock(face, r, size, cur, posn)
			postImage(server, r, img)
			prev = cur
		}
		var delay time.Duration
		if subsecond {
			ms := (time.Now().Nanosecond() / 100000)
			skip := 1000 - (ms % 1000)
			if skip < 50 {
				skip += 100
			}
			delay = time.Duration(skip) * 100 * time.Microsecond
		} else {
			ms := (time.Now().Nanosecond() / 1000000)
			skip := 1000 - (ms % 1000)
			if skip < 50 {
				skip += 100
			}
			delay = time.Duration(skip) * time.Millisecond
		}
		time.Sleep(delay)
	}

}

func postImage(url string, g image.Rectangle, img []byte) {
	paint := fmt.Sprintf("%s/paint?x=%d&y=%d", url, g.Min.X, g.Min.Y)
	http.Post(paint, "image/png", bytes.NewReader(img))
	http.Get(url + "/flush")
}

type Positioner interface {
	Position(text string) fixed.Point26_6
}

type centerPosition struct {
	face font.Face
	r    image.Rectangle
	at   *fixed.Point26_6
}

func (cp *centerPosition) Position(text string) fixed.Point26_6 {
	if cp.at != nil {
		return *cp.at
	}

	b, _ := font.BoundString(cp.face, text)
	fmt.Printf("bounds are %s\n", b)
	w := b.Max.X - b.Min.X
	h := b.Max.Y - b.Min.Y
	W := fixed.I(cp.r.Dx())
	H := fixed.I(cp.r.Dy())

	cp.at = &fixed.Point26_6{
		X: (W-w)/2 - b.Min.X,
		Y: (H-h)/2 - b.Min.Y,
	}
	fmt.Printf(" thus, draw at: %s\n", *cp.at)
	return *cp.at
}

type simplePosition struct {
	r image.Rectangle
}

func (sp simplePosition) Position(text string) fixed.Point26_6 {
	return fixed.P(6, sp.r.Dy()-4)
}

func renderClock(face font.Face, r image.Rectangle, size int, text string, posn Positioner) []byte {

	f := image.Rect(0, 0, r.Dx(), r.Dy())

	dst := image.NewGray(f)

	bg := color.Gray{0}
	fg := color.Gray{255}

	draw.Draw(dst, f, image.NewUniform(bg), image.ZP, draw.Over)

	d := &font.Drawer{
		Dst:  dst,
		Src:  image.NewUniform(fg),
		Face: face,
		Dot:  posn.Position(text),
	}
	/*
		if center {
			d.Dot = fixed.P(0, 0)
			b, _ := d.BoundString(text)
			fmt.Printf("bounds are %s\n", b)
			w := b.Max.X - b.Min.X
			h := b.Max.Y - b.Min.Y
			W := fixed.I(r.Dx())
			H := fixed.I(r.Dy())

			d.Dot = fixed.Point26_6{
				X: (W-w)/2 - b.Min.X,
				Y: (H-h)/2 - b.Min.Y,
			}
			fmt.Printf(" thus, draw at: %s\n", d.Dot)
		}*/
	d.DrawString(text)

	buf := &bytes.Buffer{}
	png.Encode(buf, dst)
	return buf.Bytes()
}
