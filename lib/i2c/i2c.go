// Package i2c provides an interface to manage the linux i2c bus.
package i2c

import (
	"fmt"
	"os"
	"syscall"
)

// Device represents a single device on a particular I2C bus
type Device struct {
	fd *os.File
}

const (
	i2c_slave = 0x0703
)

func New(addr uint8, bus int) (*Device, error) {
	devFile := fmt.Sprintf("/dev/i2c-%d", bus)

	fd, err := os.OpenFile(devFile, os.O_RDWR, 0600)
	if err != nil {
		return nil, err
	}

	_, _, rc := syscall.Syscall6(
		syscall.SYS_IOCTL,
		fd.Fd(),
		i2c_slave,
		uintptr(addr),
		0,
		0,
		0)
	if rc != 0 {
		return nil, err
	}

	return &Device{fd}, nil
}


func (d *Device) Write(buf []byte) (int, error) {
	return d.fd.Write(buf)
}
