package display

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/dkolbly/logging"
)

var log = logging.New("disp")

type DisplayClient struct {
	url string
}

func (d *DisplayClient) Size() image.Point {
	return image.Point{
		X: 320,
		Y: 240,
	}
}

func NewDisplayClient(url string) *DisplayClient {
	if url == "" {
		url = os.Getenv("DISPLAY_URL")
	}
	if url == "" {
		url = "http://127.0.0.1:8002"
	}
	if strings.HasSuffix(url, "/") {
		url = url[:len(url)-1]
	}

	return &DisplayClient{
		url: url,
	}
}

func (d *DisplayClient) Enable(ctx context.Context) {
	http.Get(d.url + "/enable")
}

func (d *DisplayClient) Disable(ctx context.Context) {
	http.Get(d.url + "/disable")
}

type Event struct {
	Time time.Time `json:"time"`
	X    int       `json:"x"`
	Y    int       `json:"y"`
	Z    int       `json:"z"`
}

var errUnexpectedStatus = errors.New("unexpected status")

func (d *DisplayClient) NextEvent(ctx context.Context) (*Event, error) {
	req, _ := http.NewRequest(http.MethodGet, d.url+"/event", nil)
	req = req.WithContext(ctx)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		log.Errorf(ctx, "Unexpected status from /event: %s",
			resp.Status)
		return nil, errUnexpectedStatus
	}
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Errorf(ctx, "Failed to read respond body: %s", err)
		return nil, err
	}
	e := &Event{}
	err = json.Unmarshal(buf, e)
	if err != nil {
		log.Errorf(ctx, "Failed to unmarshal event: %s\n------\n%s",
			err, buf)
		return nil, err
	}
	return e, nil
}

func (d *DisplayClient) GetClick(ctx context.Context) (*Event, error) {
	// get the first event
	first, err := d.NextEvent(ctx)
	if err != nil {
		return nil, err
	}
	log.Debugf(ctx, "first is: %#v", first)

	// now wait for the fingerup event, which is either a timeout
	// or a Z=0 event

	dt := 100 * time.Millisecond
	for {
		timeout, _ := context.WithTimeout(ctx, dt)
		ev, err := d.NextEvent(timeout)
		if err != nil {
			if ue, ok := err.(*url.Error); ok {
				log.Debugf(ctx, "UE %T", ue.Err)
				if ue.Err == context.DeadlineExceeded {
					log.Debugf(ctx, "Timed out")
					return first, nil
				}
			}
			log.Errorf(ctx, "Crud %T %s", err, err)
			return nil, err
		}
		log.Debugf(ctx, "subseq is: %#v", ev)
		if ev.Z == 0 {
			break
		}
	}
	return first, nil
}

func (d *DisplayClient) PaintPNG(data []byte, at image.Point) {
	q := url.Values{}
	if at.X != 0 {
		q["x"] = []string{strconv.FormatInt(int64(at.X), 10)}
	}
	if at.Y != 0 {
		q["y"] = []string{strconv.FormatInt(int64(at.X), 10)}
	}
	var u string
	if len(q) > 0 {
		u = fmt.Sprintf("%s/paint?%s", d.url, q.Encode())
	} else {
		u = d.url + "/paint"
	}
	http.Post(u, "image/png", bytes.NewReader(data))
}

func (d *DisplayClient) Flush() {
	http.Get(d.url + "/flush")
}

type CalibrationData struct {
	Matrix [6]float64 `json:"matrix"`
}

func (d *DisplayClient) TouchCalibration(cal [6]float64) {
	buf, _ := json.Marshal(&CalibrationData{Matrix: cal})
	u := d.url + "/calibration"
	http.Post(u, "application/binary", bytes.NewReader(buf))
}
