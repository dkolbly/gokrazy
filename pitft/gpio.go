package main

import (
	"fmt"
	"os"
	"syscall"
	"time"
	"unsafe"
)

type Pin struct {
	owner  *GPIO
	Number int
}

func (g *GPIO) Open(pin int) *Pin {

	f, err := os.OpenFile("/sys/class/gpio/export", os.O_WRONLY, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.Write([]byte(fmt.Sprintf("%d", pin)))

	return &Pin{
		owner:  g,
		Number: pin,
	}
}

func (p *Pin) Close() {
	f, err := os.OpenFile("/sys/class/gpio/unexport", os.O_WRONLY, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	f.Write([]byte(fmt.Sprintf("%d", p.Number)))
}

type Pull int

const (
	PullNone = Pull(iota)
	PullDown
	PullUp
)

func (p *Pin) SetPull(pup Pull) {
	p.owner.setPull(p.Number, pup)
}

const (
	bcm_gppud     = 0x94
	bcm_gppudclk0 = 0x98
	bcm_gppudclk1 = 0x9C
)

func wait150() {
	time.Sleep(time.Microsecond)
}

// wow, seriously?  c.f. BCM2835-ARM-Peripherals PDF, page 101
// which describes the register protocol for changing pull up/downs
func (g *GPIO) setPull(pin int, pup Pull) {
	pud := (*uint32)(unsafe.Pointer(&g.gpio[bcm_gppud]))

	bank := pin / 32
	bit := pin % 32
	pudclk := (*uint32)(unsafe.Pointer(&g.gpio[bcm_gppudclk0+4*bank]))

	// 1. Write GPPUD to say what we're setting
	*pud = uint32(pup)

	// 2. wait 150 cycles
	wait150()

	// 3. Write to GPPUDCLK0/1 to clock the setting in
	*pudclk = 1 << uint(bit)

	// 4. wait again
	wait150()

	// 5. remove the control signal
	*pud = 0

	// 6. remove the clock (strange that they remove the clock
	// after removing the control signal...?)
	*pudclk = 0
}

func (p *Pin) SetInput(input bool) {
	file := fmt.Sprintf("/sys/class/gpio/gpio%d/direction", p.Number)

	var f *os.File
	for {
		var err error
		f, err = os.OpenFile(file, os.O_WRONLY, 0666)
		if err == nil {
			break
		}
		fmt.Printf("fail: %s\n", err)
		time.Sleep(20 * time.Millisecond)
	}
	defer f.Close()

	if input {
		f.Write([]byte("in"))
	} else {
		f.Write([]byte("out"))
	}
}

type Edge int

const (
	EdgeNone = Edge(iota)
	EdgeRising
	EdgeFalling
	EdgeBoth
)

var edgename = map[Edge][]byte{
	EdgeNone:    []byte("none"),
	EdgeRising:  []byte("rising"),
	EdgeFalling: []byte("falling"),
	EdgeBoth:    []byte("both"),
}

func (p *Pin) SetEdge(e Edge) {
	file := fmt.Sprintf("/sys/class/gpio/gpio%d/edge", p.Number)

	f, err := os.OpenFile(file, os.O_WRONLY, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.Write(edgename[e])
}

func (p *Pin) Wait() error {
	file := fmt.Sprintf("/sys/class/gpio/gpio%d/value", p.Number)
	f, err := os.OpenFile(file, os.O_RDONLY, 0)
	if err != nil {
		return err
	}
	defer f.Close()

	// we have to manage our own epoll loop because of the
	// weird flags needed for gpio edge events
	epfd, err := syscall.EpollCreate1(syscall.EPOLL_CLOEXEC)
	if err != nil {
		return err
	}
	defer syscall.Close(epfd)

	event := syscall.EpollEvent{
		Events: syscall.EPOLLIN | syscall.EPOLLPRI | 0x80000000,
		Fd:     int32(f.Fd()),
	}

	err = syscall.EpollCtl(epfd, syscall.EPOLL_CTL_ADD, int(f.Fd()), &event)
	if err != nil {
		return err
	}

	var buf [1]syscall.EpollEvent
	first := true

	for {
		n, err := syscall.EpollWait(epfd, buf[:], 1000)
		if n == 0 {
			// got nothing... just timed out
		} else if err != nil {
			return err
		} else if first {
			fmt.Printf("ignoring level set\n")
			first = false
		} else {
			/*
				// rewind to re-read the data
				syscall.Seek(int(f.Fd()), 0, 0)

				var tmp [1]byte
				n, _ := syscall.Read(int(x.Fd), tmp[:])
			*/
			return nil
		}
	}
}
