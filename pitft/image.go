package main

import (
	"image"
	"image/color"
	"math"
)

// implements image.Image

func (d *Driver) ColorModel() color.Model {
	return color.RGBAModel
}

func (d *Driver) Bounds() image.Rectangle {
	return image.Rectangle{
		Min: image.Point{0, 0},
		Max: image.Point{WIDTH, HEIGHT},
	}
}

func (d *Driver) Set(x, y int, c color.Color) {
	if x < 0 || x >= WIDTH {
		return
	}
	if y < 0 || y >= HEIGHT {
		return
	}
	i := (x + y*WIDTH) * 2

	r, g, b, _ := c.RGBA()
	// 16 bit color values coming back
	r5 := r >> (8 + 3)
	b5 := b >> (8 + 3)
	g6 := g >> (8 + 2)

	// loG               hiG
	// GGGB BBBB   RRRR RGGG

	hi := uint8(b5) + (uint8(g6&0x7) << 5)
	lo := uint8(r5<<3) + uint8(g6>>3)
	d.pixels[i+1] = hi
	d.pixels[i] = lo
}

func (d *Driver) At(x, y int) color.Color {
	if x < 0 || x >= WIDTH {
		return color.Gray{}
	}
	if y < 0 || y >= HEIGHT {
		return color.Gray{}
	}
	hi := d.pixels[2*(x+y*WIDTH)+1]
	lo := d.pixels[2*(x+y*WIDTH)]

	r5 := lo >> 3                        // 5 bits
	b5 := hi & 0x1F                      // 5 bits
	g6 := (lo >> 5) + ((hi << 3) & 0x38) // 6 bits

	return color.RGBA{
		R: fivetoeight[r5],
		G: sixtoeight[g6],
		B: fivetoeight[b5],
	}
}

var fivetoeight [32]uint8
var sixtoeight [64]uint8

func init() {
	for i := 0; i < 32; i++ {
		fivetoeight[i] = uint8(math.Round(255 * float64(i) / 31))
	}
	for i := 0; i < 64; i++ {
		sixtoeight[i] = uint8(math.Round(255 * float64(i) / 63))
	}
}
