package main

import (
	"syscall"
	"unsafe"
)

const (
	spi_cpha = 0x01 // PHASE
	spi_cpol = 0x02 // POLARITY

	spi_mode_0 = (0 | 0)
	spi_mode_1 = (0 | spi_cpha)
	spi_mode_2 = (spi_cpol | 0)
	spi_mode_3 = (spi_cpol | spi_cpha)

	spi_cs_high = 0x04
)

const (
	ioc_NRBITS   = 8
	ioc_TYPEBITS = 8
	ioc_SIZEBITS = 14

	//
	//  DIR                  SIZE            TYPE     NUM
	// +-------+--------------------------+---------+--------+
	// | 31 30 | 29                    16 | 15     8|7      0|
	// +-------+--------------------------+---------+--------+

	ioc_NRSHIFT   = 0
	ioc_TYPESHIFT = (ioc_NRSHIFT + ioc_NRBITS)     // 8
	ioc_SIZESHIFT = (ioc_TYPESHIFT + ioc_TYPEBITS) // 16
	ioc_DIRSHIFT  = (ioc_SIZESHIFT + ioc_SIZEBITS) // 30

	ioc_WRITE = 1
	ioc_READ  = 2
)

const spi_ioc_magic = 'k'

var spi_ioc_wr_mode = iow(spi_ioc_magic, 1, 1)
var spi_ioc_rd_max_speed_hz = ior(spi_ioc_magic, 4, 4)
var spi_ioc_wr_max_speed_hz = iow(spi_ioc_magic, 4, 4)
var spi_ioc_message1 = spi_ioc_message(1)
var spi_ioc_rd_bits_per_word = ior(spi_ioc_magic, 3, 1)
var spi_ioc_wr_bits_per_word = iow(spi_ioc_magic, 3, 1)

func spi_ioc_message(n uint) uint {
	nbytes := n * uint(unsafe.Sizeof(spi_ioc_transfer{}))
	if nbytes >= (1 << ioc_SIZEBITS) {
		panic("message too big")
	}
	return iow(spi_ioc_magic, 0, nbytes)
}

//#define SPI_IOC_MESSAGE(N) _IOW(SPI_IOC_MAGIC, 0, char[SPI_MSGSIZE(N)])

func iow(typ, nr, size uint) uint {
	return ioc(ioc_WRITE, typ, nr, size)
}

func ior(typ, nr, size uint) uint {
	return ioc(ioc_READ, typ, nr, size)
}

func ioc(dir, typ, nr, size uint) uint {
	return (dir << ioc_DIRSHIFT) |
		(typ << ioc_TYPESHIFT) |
		(nr << ioc_NRSHIFT) |
		(size << ioc_SIZESHIFT)
}

func (d *SPI) ioctl(cmd uint, arg uintptr) error {
	_, _, errno := syscall.Syscall(
		syscall.SYS_IOCTL,
		uintptr(d.spi.Fd()),
		uintptr(cmd),
		arg,
	)
	if errno != 0 {
		return errno
	}
	return nil
}

/*
 * struct spi_ioc_transfer - describes a single SPI transfer
 * @tx_buf: Holds pointer to userspace buffer with transmit data, or null.
 *	If no data is provided, zeroes are shifted out.
 * @rx_buf: Holds pointer to userspace buffer for receive data, or null.
 * @len: Length of tx and rx buffers, in bytes.
 * @speed_hz: Temporary override of the device's bitrate.
 * @bits_per_word: Temporary override of the device's wordsize.
 * @delay_usecs: If nonzero, how long to delay after the last bit transfer
 *	before optionally deselecting the device before the next transfer.
 * @cs_change: True to deselect device before starting the next transfer.
 */

type spi_ioc_transfer struct {
	tx_buf        uint64 // 8
	rx_buf        uint64 // 8
	len           uint32 // 4
	speed_hz      uint32 // 4
	delay_usecs   uint16 // 2
	bits_per_word uint8  // 1
	cs_change     uint8  // 1
	tx_nbits      uint8  // 1
	rx_nbits      uint8  // 1
	pad           uint16 // 2
}
