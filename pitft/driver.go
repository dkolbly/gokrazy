package main

import (
	//"sync/atomic"
	//"math/rand"
	"fmt"
	"os"
	"syscall"
	"time"
	"unsafe"
)

const (
	PI3_GPIO_BASE = 0x3F000000 + 0x200000
)

// c.f. https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf
// and https://cdn-shop.adafruit.com/datasheets/ILI9341.pdf
//

// Resistive touchscreen controller:
//    https://cdn-shop.adafruit.com/datasheets/STMPE610.pdf
//

const (
	ILI9341_NOP     = 0x00
	ILI9341_SWRESET = 0x01
	ILI9341_RDDID   = 0x04
	ILI9341_RDDST   = 0x09

	ILI9341_SLPIN  = 0x10
	ILI9341_SLPOUT = 0x11
	ILI9341_PTLON  = 0x12
	ILI9341_NORON  = 0x13

	ILI9341_RDMODE     = 0x0A
	ILI9341_RDMADCTL   = 0x0B
	ILI9341_RDPIXFMT   = 0x0C
	ILI9341_RDIMGFMT   = 0x0D
	ILI9341_RDSELFDIAG = 0x0F

	ILI9341_INVOFF   = 0x20
	ILI9341_INVON    = 0x21
	ILI9341_GAMMASET = 0x26
	ILI9341_DISPOFF  = 0x28
	ILI9341_DISPON   = 0x29

	ILI9341_CASET = 0x2A
	ILI9341_PASET = 0x2B
	ILI9341_RAMWR = 0x2C
	ILI9341_RAMRD = 0x2E

	ILI9341_PTLAR    = 0x30
	ILI9341_MADCTL   = 0x36
	ILI9341_VSCRSADD = 0x37
	ILI9341_PIXFMT   = 0x3A

	ILI9341_DISPBRT = 0x51 // Write Display Brightness (51h)

	ILI9341_FRMCTR1 = 0xB1
	ILI9341_FRMCTR2 = 0xB2
	ILI9341_FRMCTR3 = 0xB3
	ILI9341_INVCTR  = 0xB4
	ILI9341_DFUNCTR = 0xB6

	ILI9341_PWCTR1 = 0xC0
	ILI9341_PWCTR2 = 0xC1
	ILI9341_PWCTR3 = 0xC2
	ILI9341_PWCTR4 = 0xC3
	ILI9341_PWCTR5 = 0xC4
	ILI9341_VMCTR1 = 0xC5
	ILI9341_VMCTR2 = 0xC7

	ILI9341_RDID1 = 0xDA
	ILI9341_RDID2 = 0xDB
	ILI9341_RDID3 = 0xDC
	ILI9341_RDID4 = 0xDD

	ILI9341_GMCTRP1 = 0xE0
	ILI9341_GMCTRN1 = 0xE1

	MADCTL_MY  = 0x80
	MADCTL_MX  = 0x40
	MADCTL_MV  = 0x20
	MADCTL_ML  = 0x10
	MADCTL_RGB = 0x00
	MADCTL_BGR = 0x08
	MADCTL_MH  = 0x04

	// CONFIGURATION AND GLOBAL STUFF ------------------------------

	DC_PIN  = 25
	BITRATE = 80000000
	//BITRATE = 500000
	//SPI_MODE = SPI_MODE_0
	FPS = 30

	// From GPIO example code by Dom and Gert van Loo on elinux.org:
	//PI1_BCM2708_PERI_BASE = 0x20000000
	//PI1_GPIO_BASE         = (PI1_BCM2708_PERI_BASE + 0x200000)
	//PI2_BCM2708_PERI_BASE = 0x3F000000
	//PI2_GPIO_BASE         = (PI2_BCM2708_PERI_BASE + 0x200000)
	BLOCK_SIZE = (4 * 1024)
)

type Driver struct {
	pixels []byte
	*GPIO
	*SPI
}

type GPIO struct {
	gpio       []byte
	set, clear *uint32
}

type SPI struct {
	spi *os.File
	hz  uint32
}

func OpenGPIO() *GPIO {
	// first, let's mmap the SPI transfer block memory
	fd, err := os.OpenFile("/dev/mem", os.O_RDWR|os.O_SYNC, 0666)
	if err != nil {
		panic(err)
	}
	defer fd.Close()

	data, err := syscall.Mmap(
		int(fd.Fd()),
		PI3_GPIO_BASE,
		BLOCK_SIZE,
		syscall.PROT_READ|syscall.PROT_WRITE,
		syscall.MAP_SHARED,
	)
	if err != nil {
		panic(err)
	}
	fmt.Printf("mmapped at %p\n", unsafe.Pointer(&data[0]))

	return &GPIO{
		gpio:  data,
		set:   (*uint32)(unsafe.Pointer(&data[0x1c])),
		clear: (*uint32)(unsafe.Pointer(&data[0x28])),
	}
}

func OpenSPI(major, minor int, hz uint32) *SPI {
	// next, let's get to the SPI device so we can ioctl() it
	spi, err := os.OpenFile(
		fmt.Sprintf("/dev/spidev%d.%d", major, minor),
		os.O_WRONLY|syscall.O_NONBLOCK,
		0)
	if err != nil {
		panic(err)
	}

	fmt.Printf("opened SPI dev (%d.%d)\n", major, minor)
	s := &SPI{
		spi: spi,
		hz:  hz,
	}
	s.SetMaxSpeedHz(hz)
	return s
}

func (s *SPI) SetMaxSpeedHz(hz uint32) {
	err := s.ioctl(spi_ioc_wr_max_speed_hz, uintptr(unsafe.Pointer(&hz)))
	if err != nil {
		panic(err)
	}
}

func (s *SPI) SetMode(mode int) {
	m := byte(mode)
	err := s.ioctl(spi_ioc_wr_mode, uintptr(unsafe.Pointer(&m)))
	if err != nil {
		panic(err)
	}
}

func New(w, h int) (*Driver, error) {

	fmt.Printf("sizeof(ioc transfer) = %d\n",
		unsafe.Sizeof(spi_ioc_transfer{}))

	fmt.Printf("mmapping file...\n")

	d := &Driver{
		pixels: make([]byte, w*h*2),
		GPIO:   OpenGPIO(),
		SPI:    OpenSPI(0, 0, BITRATE),
	}
	if dc_pin > 31 {
		panic("driver set/clear only handles up to GPIO31")
	}
	// set up some settings

	fmt.Printf("setting: spi_mode_0\n")

	d.SetMode(spi_mode_0)
	/*var mode byte = spi_mode_0
	err := d.ioctl(spi_ioc_wr_mode, uintptr(unsafe.Pointer(&mode)))
	if err != nil {
		panic(err)
	}*/

	var s0 uint32
	err := d.ioctl(spi_ioc_rd_max_speed_hz, uintptr(unsafe.Pointer(&s0)))
	if err != nil {
		panic(err)
	}
	fmt.Printf("checking: max_speed %d\n", s0)

	fmt.Printf("setting: max_speed_hz\n")

	d.SetMaxSpeedHz(BITRATE)

	err = d.ioctl(spi_ioc_rd_max_speed_hz, uintptr(unsafe.Pointer(&s0)))
	if err != nil {
		panic(err)
	}
	fmt.Printf("checking: max_speed %d\n", s0)

	// set DC as output pin (but have to set INP before OUT)
	fmt.Printf("setting: dc_pin input\n")
	d.set_input_gpio(dc_pin)

	fmt.Printf("setting: dc_pin output\n")
	d.set_output_gpio(dc_pin)

	fmt.Printf("Read Display Identification Information\n")
	d.writeCommand(ILI9341_RDDID)
	buf := d.readData(4)
	fmt.Printf("==> %x\n", buf)

	fmt.Printf("setting 0xEF\n")
	d.writeCommand(0xEF)
	d.writeData(0x03)
	d.writeData(0x80)
	d.writeData(0x02)

	d.writeCommand(0xCF)
	d.writeData(0x00)
	d.writeData(0XC1)
	d.writeData(0X30)

	d.writeCommand(0xED)
	d.writeData(0x64)
	d.writeData(0x03)
	d.writeData(0X12)
	d.writeData(0X81)

	d.writeCommand(0xE8)
	d.writeData(0x85)
	d.writeData(0x00)
	d.writeData(0x78)

	d.writeCommand(0xCB)
	d.writeData(0x39)
	d.writeData(0x2C)
	d.writeData(0x00)
	d.writeData(0x34)
	d.writeData(0x02)

	d.writeCommand(0xF7)
	d.writeData(0x20)

	d.writeCommand(0xEA)
	d.writeData(0x00)
	d.writeData(0x00)

	d.writeCommand(ILI9341_PWCTR1) //Power control
	d.writeData(0x23)              //VRH[5:0]

	d.writeCommand(ILI9341_PWCTR2) //Power control
	d.writeData(0x10)              //SAP[2:0];BT[3:0]

	d.writeCommand(ILI9341_VMCTR1) //VCM control
	d.writeData(0x3e)
	d.writeData(0x28)

	d.writeCommand(ILI9341_VMCTR2) //VCM control2
	d.writeData(0x86)              //--

	d.writeCommand(ILI9341_MADCTL) // Memory Access Control
	d.writeData(MADCTL_MV | MADCTL_BGR)

	d.writeCommand(ILI9341_VSCRSADD) // Vertical scroll
	d.writeData16(0)                 // Zero

	d.writeCommand(ILI9341_PIXFMT)
	d.writeData(0x55) // 0x5 = 16 bpp, (0x55 = 16 bpp both MCU and RGB iface)

	d.writeCommand(ILI9341_FRMCTR1)
	d.writeData(0x00)
	d.writeData(0x18)

	d.writeCommand(ILI9341_DFUNCTR) // Display Function Control
	d.writeData(0x08)
	d.writeData(0x82)
	d.writeData(0x27)

	d.writeCommand(0xF2) // 3Gamma Function Disable
	d.writeData(0x00)

	d.writeCommand(ILI9341_GAMMASET) //Gamma curve selected
	d.writeData(0x01)

	d.writeCommand(ILI9341_GMCTRP1) //Set Gamma
	d.writeData(0x0F)
	d.writeData(0x31)
	d.writeData(0x2B)
	d.writeData(0x0C)
	d.writeData(0x0E)
	d.writeData(0x08)
	d.writeData(0x4E)
	d.writeData(0xF1)
	d.writeData(0x37)
	d.writeData(0x07)
	d.writeData(0x10)
	d.writeData(0x03)
	d.writeData(0x0E)
	d.writeData(0x09)
	d.writeData(0x00)

	d.writeCommand(ILI9341_GMCTRN1) //Set Gamma
	d.writeData(0x00)
	d.writeData(0x0E)
	d.writeData(0x14)
	d.writeData(0x03)
	d.writeData(0x11)
	d.writeData(0x07)
	d.writeData(0x31)
	d.writeData(0xC1)
	d.writeData(0x48)
	d.writeData(0x08)
	d.writeData(0x0F)
	d.writeData(0x0C)
	d.writeData(0x31)
	d.writeData(0x36)
	d.writeData(0x0F)

	//d.writeCommand(ILI9341_SLPOUT) //Exit Sleep
	//time.Sleep(120 * time.Millisecond)

	d.writeCommand(ILI9341_DISPON) //Display on
	time.Sleep(120 * time.Millisecond)

	fmt.Printf("ready!\n")
	return d, nil
}

func (d *Driver) setCommand() {
	//atomic.StoreUint32(d.clear, dc_mask)
	*d.clear = dc_mask
}

func (d *Driver) setData() {
	*d.set = dc_mask
}

func (d *Driver) writeCommand(cmd uint8) {
	d.setCommand()
	tx := spi_ioc_transfer{
		tx_buf:        uint64(uintptr(unsafe.Pointer(&cmd))),
		len:           1,
		rx_buf:        0,
		delay_usecs:   0,
		bits_per_word: 8,
		pad:           0,
		speed_hz:      BITRATE,
		tx_nbits:      0,
		rx_nbits:      0,
		cs_change:     0,
	}
	d.ioctl(spi_ioc_message1, uintptr(unsafe.Pointer(&tx)))
}

func (d *Driver) writeData(b uint8) {
	d.setData()
	tx := spi_ioc_transfer{
		tx_buf:        uint64(uintptr(unsafe.Pointer(&b))),
		len:           1,
		rx_buf:        0,
		delay_usecs:   0,
		bits_per_word: 8,
		pad:           0,
		speed_hz:      BITRATE,
		tx_nbits:      0,
		rx_nbits:      0,
		cs_change:     0,
	}
	d.ioctl(spi_ioc_message1, uintptr(unsafe.Pointer(&tx)))
}

func (d *Driver) readData(n int) []byte {
	buf := make([]byte, n)
	tx := spi_ioc_transfer{
		len:           uint32(n),
		rx_buf:        uint64(uintptr(unsafe.Pointer(&buf[0]))),
		delay_usecs:   0,
		bits_per_word: 8,
		pad:           0,
		speed_hz:      BITRATE,
		tx_nbits:      0,
		rx_nbits:      0,
		cs_change:     0,
	}
	d.ioctl(spi_ioc_message1, uintptr(unsafe.Pointer(&tx)))
	return buf
}

func (d *Driver) writeDataBytes(buf []uint8) {
	d.setData()
	tx := spi_ioc_transfer{
		tx_buf:        uint64(uintptr(unsafe.Pointer(&buf[0]))),
		len:           uint32(len(buf)),
		rx_buf:        0,
		delay_usecs:   0,
		bits_per_word: 8,
		pad:           0,
		speed_hz:      BITRATE,
		tx_nbits:      0,
		rx_nbits:      0,
		cs_change:     0,
	}
	d.ioctl(spi_ioc_message1, uintptr(unsafe.Pointer(&tx)))
}

func (d *Driver) writeData16(value uint16) {
	d.setData()
	chunk := [2]uint8{
		uint8(value >> 8),
		uint8(value & 0xff),
	}

	tx := spi_ioc_transfer{
		tx_buf:        uint64(uintptr(unsafe.Pointer(&chunk[0]))),
		len:           2,
		rx_buf:        0,
		delay_usecs:   0,
		bits_per_word: 8,
		pad:           0,
		speed_hz:      BITRATE,
		tx_nbits:      0,
		rx_nbits:      0,
		cs_change:     0,
	}
	d.ioctl(spi_ioc_message1, uintptr(unsafe.Pointer(&tx)))
}

func (d *Driver) writeData32(value uint32) {
	d.setData()
	chunk := [4]uint8{
		uint8(value >> 24),
		uint8(value >> 16),
		uint8(value >> 8),
		uint8(value),
	}

	tx := spi_ioc_transfer{
		tx_buf:        uint64(uintptr(unsafe.Pointer(&chunk[0]))),
		len:           4,
		rx_buf:        0,
		delay_usecs:   0,
		bits_per_word: 8,
		pad:           0,
		speed_hz:      BITRATE,
		tx_nbits:      0,
		rx_nbits:      0,
		cs_change:     0,
	}
	d.ioctl(spi_ioc_message1, uintptr(unsafe.Pointer(&tx)))
}

func (d *GPIO) set_input_gpio(pin uint) {
	word := pin / 10
	index := pin % 10

	var reg *uint32
	reg = (*uint32)(unsafe.Pointer(&d.gpio[word*4]))
	mask := ^(uint32(7) << (index * 3))
	old := *reg // old := atomic.LoadUint32(reg)
	fill := old & mask
	*reg = fill // atomic.StoreUint32(reg, fill)
}

func (d *GPIO) set_output_gpio(pin uint) {
	word := pin / 10
	index := pin % 10

	var reg *uint32
	reg = (*uint32)(unsafe.Pointer(&d.gpio[word*4]))
	old := *reg
	fill := old | (1 << (index * 3))
	*reg = fill // atomic.StoreUint32(reg, fill)
}

//#define INP_GPIO(g)          *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
//#define OUT_GPIO(g)          *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))

const dc_pin = DC_PIN
const dc_mask = 1 << dc_pin

func (d *Driver) Flush() {

	// the actual parameter is
	//
	//    START HI      START LO         END HI         END LO
	// +--------------+--------------+--------------+--------------+
	// |              |              |              |              |
	// +--------------+--------------+--------------+--------------+
	//
	// so when we pass WIDTH-1 here, we are actually saying the
	// `start` is 0 and the `end is WIDTH-1
	//
	d.writeCommand(ILI9341_CASET) // Column addr set
	d.writeData32(WIDTH - 1)
	//d.writeData32(10);

	d.writeCommand(ILI9341_PASET) // Row ("Page Address") addr set
	d.writeData32(HEIGHT - 1)
	//d.writeData32(10);

	d.writeCommand(ILI9341_RAMWR) // write to RAM

	buf := d.pixels
	for len(buf) > 0 {
		chunk := len(buf)
		if chunk > BLOCK_SIZE {
			chunk = BLOCK_SIZE
		}
		d.writeDataBytes(buf[:chunk])
		buf = buf[chunk:]
	}
}

func (d *Driver) Off() {
	d.writeCommand(ILI9341_SLPIN)   // Sleep enter
	d.writeCommand(ILI9341_DISPOFF) //Display off
}

func (d *Driver) On() {
	d.writeCommand(ILI9341_SLPOUT) // Sleep exit
	time.Sleep(20 * time.Millisecond)
	d.writeCommand(ILI9341_DISPON) //Display on
	time.Sleep(120 * time.Millisecond)
}
