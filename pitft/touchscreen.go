package main

import (
	"fmt"
	"time"
	"unsafe"
)

type Touch struct {
	*SPI
	intr *Pin
	cal *CalibrationData
}

type TouchEvent struct {
	T time.Time `json:"time"`
	X int       `json:"x"`
	Y int       `json:"y"`
	Z int       `json:"z"`
}

const irq_pin = 24

func (s *SPI) RW(data []byte) []byte {
	rx := make([]byte, len(data))
	tx := spi_ioc_transfer{
		tx_buf:        uint64(uintptr(unsafe.Pointer(&data[0]))),
		len:           uint32(len(data)),
		rx_buf:        uint64(uintptr(unsafe.Pointer(&rx[0]))),
		delay_usecs:   0,
		bits_per_word: 8,
		pad:           0,
		speed_hz:      s.hz,
		tx_nbits:      0,
		rx_nbits:      0,
		cs_change:     0,
	}
	s.ioctl(spi_ioc_message1, uintptr(unsafe.Pointer(&tx)))
	return rx
}

func (s *SPI) readUint8(addr uint8) uint8 {
	return s.RW([]byte{0x80 + addr, 0})[1]
}

func OpenTouch() *Touch {
	g := OpenGPIO()
	pin := g.Open(irq_pin)
	pin.SetInput(true)
	pin.SetEdge(EdgeFalling)
	pin.SetPull(PullUp)

	s := OpenSPI(0, 1, 1000000)

	s.SetMode(spi_mode_1)

	// read the chip ID

	var bpw uint8
	s.ioctl(spi_ioc_rd_bits_per_word, uintptr(unsafe.Pointer(&bpw)))
	fmt.Printf("bits per word = %d\n", bpw)

	r0 := s.RW([]byte{0x80, 0})
	fmt.Printf("R0 %#v\n", r0)
	r1 := s.RW([]byte{0x81, 0})
	fmt.Printf("R1 %#v\n", r1)

	deviceID := (int(r0[1]) << 8) + int(r1[1])
	fmt.Printf("Device id is %#x\n", deviceID)
	if deviceID != 0x811 {
		// TODO there is some logic in touchmouse.py to try a
		// different SPI mode in this case; we'll just panic
		// for now
		panic("device ID is not recognized")
	}
	return &Touch{
		SPI:  s,
		intr: pin,
	}
}

const (
	stmpe_sys_ctrl1      = 0x03
	stmpe_sys_ctrl2      = 0x04
	stmpe_int_ctrl       = 0x09
	stmpe_int_en         = 0x0a
	stmpe_int_sta        = 0x0b
	stmpe_gpio_set_pin   = 0x10
	stmpe_gpio_clr_pin   = 0x11
	stmpe_gpio_dir       = 0x13
	stmpe_gpio_alt_funct = 0x17

	stmpe_adc_ctrl1      = 0x20
	stmpe_adc_ctrl2      = 0x21
	stmpe_tsc_ctrl       = 0x40
	stmpe_tsc_cfg        = 0x41
	stmpe_fifo_th        = 0x4a
	stmpe_fifo_sta       = 0x4b
	stmpe_tsc_fraction_z = 0x56
	stmpe_tsc_i_drive    = 0x58

	stmpe_sys_ctrl1_reset    = 0x02
	stmpe_int_ctrl_pol_high  = 0x04
	stmpe_int_ctrl_edge      = 0x02
	stmpe_int_ctrl_enable    = 0x01
	stmpe_int_en_touchdet    = 0x01
	stmpe_adc_ctrl1_10bit    = 0x00
	stmpe_adc_ctrl2_6_5mhz   = 0x02
	stmpe_tsc_ctrl_en        = 0x01
	stmpe_tsc_ctrl_xyz       = 0x00
	stmpe_tsc_cfg_4sample    = 0x80
	stmpe_tsc_cfg_8sample    = 0xc0
	stmpe_tsc_cfg_delay_1ms  = 0x20
	stmpe_tsc_cfg_settle_1ms = 0x03
	stmpe_tsc_cfg_settle_5ms = 0x04
	stmpe_fifo_sta_reset     = 0x01
	stmpe_tsc_i_drive_50ma   = 0x01

	calibration_min_x = 0
	calibration_min_y = 1
	calibration_max_x = 2
	calibration_max_y = 3
)

func (s *SPI) writeUint8(addr, value uint8) {
	tmp := [2]uint8{addr, value}

	tx := spi_ioc_transfer{
		tx_buf:        uint64(uintptr(unsafe.Pointer(&tmp[0]))),
		len:           2,
		rx_buf:        0,
		delay_usecs:   0,
		bits_per_word: 8,
		pad:           0,
		speed_hz:      s.hz,
		tx_nbits:      0,
		rx_nbits:      0,
		cs_change:     0,
	}
	s.ioctl(spi_ioc_message1, uintptr(unsafe.Pointer(&tx)))

}

func (t *Touch) init() {
	// soft reset
	t.writeUint8(stmpe_sys_ctrl1, stmpe_sys_ctrl1_reset)
	time.Sleep(20 * time.Millisecond)

	// initialize

	// turn on clocks!
	t.writeUint8(stmpe_sys_ctrl2, 0x0)

	// XYZ and enable!
	t.writeUint8(stmpe_tsc_ctrl, stmpe_tsc_ctrl_xyz|stmpe_tsc_ctrl_en)

	// 96 clocks per conversion
	t.writeUint8(stmpe_adc_ctrl1, stmpe_adc_ctrl1_10bit|(0x6<<4))
	t.writeUint8(stmpe_adc_ctrl2, stmpe_adc_ctrl2_6_5mhz)
	t.writeUint8(stmpe_tsc_cfg, stmpe_tsc_cfg_8sample|stmpe_tsc_cfg_delay_1ms|stmpe_tsc_cfg_settle_1ms)
	t.writeUint8(stmpe_tsc_fraction_z, 0x6)
	t.writeUint8(stmpe_fifo_th, 1)
	t.writeUint8(stmpe_fifo_sta, stmpe_fifo_sta_reset)
	t.writeUint8(stmpe_fifo_sta, 0) // unreset
	t.writeUint8(stmpe_tsc_i_drive, stmpe_tsc_i_drive_50ma)
	t.writeUint8(stmpe_int_ctrl, stmpe_int_ctrl_edge|stmpe_int_ctrl_enable)
	t.writeUint8(stmpe_int_en, stmpe_int_en_touchdet)
	t.writeUint8(stmpe_int_sta, 0xff) // reset all interrupts

	// make GPIO-2 an output
	t.writeUint8(stmpe_gpio_alt_funct, 0x0f) // GPIO2 and 3 are GPIO
	t.writeUint8(stmpe_gpio_dir, 1<<2)       // and GPIO2 is an output
}

func (t *Touch) listen() <-chan TouchEvent {
	ch := make(chan TouchEvent, 10)
	go t.pump(ch)
	return ch
}

func (t *Touch) pump(ch chan<- TouchEvent) {
	defer close(ch)
	for {
		err := t.intr.Wait()
		if err != nil {
			panic(err)
		}
		num := 0
		for {
			ev, ok := t.convert()
			if !ok {
				break
			}
			ch <- ev
			num++
		}
		fmt.Printf("OK, that was a chunk of %d events\n", num)
		// clear interrupts
		t.writeUint8(stmpe_int_sta, 0xff)
	}
}

func (t *Touch) read() {
	for count := 0; count < 10; count++ {
		err := t.intr.Wait()
		if err != nil {
			panic(err)
		}
		num := 0
		for {
			ev, ok := t.convert()
			if !ok {
				break
			}
			fmt.Printf("  >>> %#v\n", ev)
			num++
		}
		fmt.Printf("OK, that was %d events\n", num)
		// clear interrupts
		t.writeUint8(stmpe_int_sta, 0xff)
	}
}

func (t *Touch) convert() (ev TouchEvent, ok bool) {
	if t.readUint8(stmpe_tsc_ctrl)&0x80 == 0 {
		return
	}

	// allow some conversion time before first reading
	time.Sleep(33 * time.Millisecond)

	var event [4]byte
	for i := range event {
		// get a byte of the event record
		event[i] = t.RW([]byte{0xd7, 0})[1]
	}

	x := (uint32(event[0]) << 4) | (uint32(event[1]) >> 4)
	y := (uint32(event[1]&0x0f) << 8) | uint32(event[2])
	z := event[3]

	//fmt.Printf("%8d %8d %8d\n", x, y, z)

	// clear FIFO
	t.writeUint8(stmpe_fifo_sta, 1)
	t.writeUint8(stmpe_fifo_sta, 0)

	ev.T = time.Now()

	x1 := float64(x)
	y1 := float64(y)

	if t.cal != nil {
		c := t.cal
		x2 := x1 * c.Matrix[0] + y1 * c.Matrix[2] + c.Matrix[4]
		y2 := x1 * c.Matrix[1] + y1 * c.Matrix[3] + c.Matrix[5]
		x1, y1 = x2, y2
	}
	
	fmt.Printf("RAW xy %d %d ==> TRANSFORMED %.3f %.3f\n", x, y, x1, y1)
		
	ev.X = int(x1)
	ev.Y = int(y1)
	ev.Z = int(z)
	ok = true
	return
}

func (t *Touch) releaseBacklight() {
	t.writeUint8(stmpe_gpio_dir, 0) // mark GPIO2 as an input
}

func (t *Touch) Backlight(enable bool) {
	if enable {
		// set GPIO2, which will drive the LED transistor
		t.writeUint8(stmpe_gpio_set_pin, 1<<2)
	} else {
		// clear GPIO2, which will put 0V on the base of the
		// LED transitor, turning off the backlight
		t.writeUint8(stmpe_gpio_clr_pin, 1<<2)
	}
}

//
// CTM:
//
// [ a_x   a_y   0 ]
// [ b_x   b_y   0 ]
// [ c_x   c_y   1 ]
//
// x' = a_x x + b_x y + c_x
// y' = a_y x + b_y y + c_y
//
// we store [ a_x a_y b_x b_y c_x c_y ] in the matrix
//
type CalibrationData struct {
	Matrix [6]float64 `json:"matrix"`
}

func (t *Touch) Calibrate(c *CalibrationData) {
	t.cal = c
}
