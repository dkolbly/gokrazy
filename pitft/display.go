package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

const (
	WIDTH  = 320
	HEIGHT = 240
)

type PiTFT struct {
	touch   *Touch
	display *Driver
	rx      <-chan TouchEvent
	dirty   image.Rectangle
}

var fullRect = image.Rectangle{
	Min: image.Point{0, 0},
	Max: image.Point{WIDTH, HEIGHT},
}

func main() {
	touch := OpenTouch()
	touch.init()
	touch.Backlight(false)

	d, err := New(WIDTH, HEIGHT)
	if err != nil {
		panic(err)
	}

	target := &PiTFT{
		touch:   touch,
		rx:      touch.listen(),
		display: d,
		dirty:   fullRect,
	}

	mux := http.NewServeMux()

	mux.HandleFunc("/disable", target.disable)
	mux.HandleFunc("/enable", target.enable)
	mux.HandleFunc("/flush", target.flush)
	mux.HandleFunc("/paint", target.paint)
	mux.HandleFunc("/erase", target.erase)
	mux.HandleFunc("/event", target.event)
	mux.HandleFunc("/calibration", target.calibration)

	http.ListenAndServe(":8002", mux)
}

func (t *PiTFT) erase(w http.ResponseWriter, r *http.Request) {
	black := color.Gray{0}

	rect := t.display.Bounds()
	draw.Draw(t.display, rect, image.NewUniform(black), image.ZP, draw.Over)
	t.dirty = rect
}

func (t *PiTFT) flush(w http.ResponseWriter, r *http.Request) {
	t0 := time.Now()
	t.display.Flush()
	fmt.Printf("Flushed frame in %s\n", time.Since(t0))
	t.dirty = image.Rectangle{}
}

func (t *PiTFT) disable(w http.ResponseWriter, r *http.Request) {
	t.touch.Backlight(false)
	t.display.Off()
}

func (t *PiTFT) enable(w http.ResponseWriter, r *http.Request) {
	t.touch.Backlight(true)
	t.display.On()
}

func (t *PiTFT) calibration(w http.ResponseWriter, r *http.Request) {
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	cb := new(CalibrationData)
	err = json.Unmarshal(buf, cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{"error":"bad calibration data"}`))
		return
	}
	t.touch.Calibrate(cb)
}



func (t *PiTFT) paint(w http.ResponseWriter, r *http.Request) {
	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	img, err := png.Decode(bytes.NewReader(buf))
	if err != nil {
		panic(err)
	}

	formInt := func(name string) int {
		s := r.FormValue(name)
		if s == "" {
			return 0
		}
		n, _ := strconv.ParseInt(s, 10, 16)
		return int(n)
	}

	at := image.Point{
		formInt("x"),
		formInt("y"),
	}
	dst := img.Bounds().Add(at)

	draw.DrawMask(t.display, dst, img, image.ZP, img, image.ZP, draw.Over)
	t.dirty = t.dirty.Union(dst)
	fmt.Printf("Damaged %s, now dirty: %s\n", dst, t.dirty)
}

func (t *PiTFT) event(w http.ResponseWriter, r *http.Request) {
	formInt := func(name string) int {
		s := r.FormValue(name)
		if s == "" {
			return 0
		}
		n, _ := strconv.ParseInt(s, 10, 16)
		return int(n)
	}

	timeout := formInt("timeout_sec")

	ctx := r.Context()
	var expire <-chan time.Time
	if timeout > 0 {
		dt := time.Second * time.Duration(timeout)
		expire = time.After(dt)
		fmt.Printf("(expire after %s)\n", dt)
	}

	select {
	case <-expire:
		fmt.Printf("pop!")
		w.Write([]byte("{}\n"))
	case <-ctx.Done():
		return
	case event := <-t.rx:
		buf, _ := json.MarshalIndent(&event, "", "  ")
		w.Write(append(buf, '\n'))
	}
}
