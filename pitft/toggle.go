package main

import (
	"fmt"
	"os"
	"syscall"
	"time"
	"unsafe"
)

func run() {

	fmt.Printf("mmapping file...\n")

	// first, let's mmap the SPI transfer block memory
	fd, err := os.OpenFile("/dev/mem", os.O_RDWR|os.O_SYNC, 0666)
	if err != nil {
		panic(err)
	}
	defer fd.Close()

	data, err := syscall.Mmap(
		int(fd.Fd()),
		PI3_GPIO_BASE,
		4*1024,
		syscall.PROT_READ|syscall.PROT_WRITE,
		syscall.MAP_SHARED,
	)
	if err != nil {
		panic(err)
	}
	fmt.Printf("mmapped at %p\n", unsafe.Pointer(&data[0]))

	paddr := (*uint32)(unsafe.Pointer(&data[0]))
	*paddr = 0x1000

	paddr1 := (*uint32)(unsafe.Pointer(&data[0x1C]))
	paddr2 := (*uint32)(unsafe.Pointer(&data[0x28]))

	for {
		*paddr1 = 0x10
		time.Sleep(250 * time.Millisecond)
		*paddr2 = 0x10
		time.Sleep(250 * time.Millisecond)
	}
}
