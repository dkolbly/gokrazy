package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

type Listing struct {
	Entries []Entry `json:"entry"`
}

type Entry struct {
	Name  string    `json:"name"`
	Size  int64     `json:"size"`
	Mode  string    `json:"mode"`
	MTime time.Time `json:"mtime"`
	IsDir bool      `json:"is_dir"`
}

type File struct {
	Entry
	Content []byte `json:"content"`
}

func Write(arg string, data []byte) interface{} {
	fi, err := os.Stat(arg)
	if err != nil {
		return &Error{
			Error: err.Error(),
		}
	}
	if fi.IsDir() {
		return &Error{
			Error: "is a directory",
		}
	}
	
	f, err := os.OpenFile(arg, os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return &Error{
			Error: err.Error(),
		}
	}
	defer f.Close()
	
	n, err := f.Write(data)
	if err != nil {
		return &Error{
			Error: err.Error(),
			Details: fmt.Sprintf("wrote only %d of %d", n, len(data)),
		}
	}
	fi2, err := os.Stat(arg)
	if err != nil {
		return &Error{
			Error: err.Error(),
			Details: "trying to re-read stats",
		}
	}

	e := fiToEntry(fi2)
	return &e
}

func Read(arg string) interface{} {
	fi, err := os.Stat(arg)
	if err != nil {
		return &Error{
			Error: err.Error(),
		}
	}

	buf, err := ioutil.ReadFile(arg)
	if err != nil {
		return &Error{
			Error: err.Error(),
		}
	}
	
	return &File{
		Entry:   fiToEntry(fi),
		Content: buf,
	}
}

func fiToEntry(fi os.FileInfo) Entry {
	return Entry{
		Name:  fi.Name(),
		Size:  fi.Size(),
		Mode:  fi.Mode().String(),
		MTime: fi.ModTime(),
		IsDir: fi.IsDir(),
	}
}

func List(arg string) interface{} {
	lst, err := ioutil.ReadDir(arg)
	if err != nil {
		return &Error{
			Error: err.Error(),
		}
	}
	l := Listing{
		Entries: make([]Entry, len(lst)),
	}
	for i, fi := range lst {
		l.Entries[i] = fiToEntry(fi)
	}
	return &l
}

/*func Label() string {
	ioutil.ReadFile("/sys/class/gpio/
}
*/
