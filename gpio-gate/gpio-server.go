package main

import (
	"encoding/json"
	"io/ioutil"
	"fmt"
	"net/http"
	"strings"
)

func main() {
	gate := &Server{}
	http.ListenAndServe(":8001", gate)
}

type Server struct {
}

type Error struct {
	code    int
	Error   string `json:"error"`
	Details string `json:"details,omitempty"`
}

func (e *Error) StatusCode() int {
	if e.code == 0 {
		return http.StatusInternalServerError
	}

	return e.code
}

type StatusCoder interface {
	StatusCode() int
}

func (srv *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var resp interface{}
	parts := strings.Split(r.URL.Path, "/")
	if parts[1] == "fs" {
		fpath := make([]string, len(parts)-1)
		copy(fpath[1:], parts[2:])
		resp = srv.serveFS(w, r, fpath)
	} else {
		resp = &Error{
			code:  http.StatusNotFound,
			Error: "not found",
		}
	}
	buf, _ := json.MarshalIndent(resp, "", "  ")

	if coder, ok := resp.(StatusCoder); ok {
		w.WriteHeader(coder.StatusCode())
	}
	w.Write(append(buf, '\n'))
}

func (srv *Server) serveFS(w http.ResponseWriter, r *http.Request, parts []string) interface{} {

	if r.Method == http.MethodPut {
		if parts[len(parts)-1] == "" {
			return &Error{
				code:    http.StatusMethodNotAllowed,
				Error:   "invalid method",
				Details: "cannot PUT on a directory",
			}
		}

		buf, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return &Error{
				Error:   err.Error(),
				Details: "trying to read body",
			}
		}
		target := strings.Join(parts, "/")
		fmt.Printf("WRITING %q with %q\n", target, buf)
		return Write(target, buf)
	}

	if r.Method != http.MethodGet {
		return &Error{
			code:    http.StatusMethodNotAllowed,
			Error:   "invalid method",
			Details: "only GET is OK",
		}
	}

	if parts[len(parts)-1] == "" {
		target := strings.Join(parts[:len(parts)-1], "/")
		fmt.Printf("LISTING %q\n", target)
		return List(target)
	} else {
		target := strings.Join(parts, "/")
		fmt.Printf("READING %q\n", target)
		return Read(target)
	}
}
