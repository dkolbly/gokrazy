package main

type SleepMode struct {
	d *DisplayClient
}

func (s *SleepMode) Start() {
	s.d.Disable()
}

func (s *SleepMode) Finish() {
	s.d.Enable()
}
