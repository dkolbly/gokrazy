package main

import (
	"context"
	"os/exec"
)

type ClockMode struct {
	d    *DisplayClient
	proc *exec.Cmd
}

func NewClockMode(d *DisplayClient) *ClockMode {
	return &ClockMode{
		d: d,
	}
}

func (c *ClockMode) Start(ctx context.Context) {
	p := exec.Command("clock",
		"--geom", "320x240",
		"--format", "15:04:05",
		"--center",
		"--size", "62")
	c.proc = p

	p.Start()
	log.Debugf(ctx, "running clock: pid %d", p.Process.Pid)
}

func (c *ClockMode) Finish(ctx context.Context) {
	c.proc.Process.Kill()
	c.proc.Wait()
}
