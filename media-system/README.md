
## Major Modes of Operation

The media controller is in one of three main states:

  - *Sleep* - in the sleep state, the display is blanked
    and the backlight is off.

  - *Clock* - in the clock state, the display shows a clock

  - *Media* - in the media state, the display shows information
    from the current state of the media server


Going from *Sleep* or *Clock* to *Media* is achieved by touching the
screen.  The screen is non-responsive to touch for 500ms after waking,
to avoid "touch through" effects.

While media is active, the system will stay in the *Media* state.

While in the Media state but media is inactive, the system will enter
the clock state automatically after 30 minutes.

From clock state, the system will enter *Sleep* state automatically
after 2 hours.

