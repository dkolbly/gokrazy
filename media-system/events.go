package main

import (
	"context"
	"time"
)

func (s *System) watch(ctx context.Context, d *DisplayClient) {
	for {
		ev, err := d.NextEvent(ctx)
		if err != nil {
			log.Debugf(ctx, "drat: %s", err)
			time.Sleep(500*time.Millisecond)
			continue
		}
		log.Debugf(ctx, "got event: %#v", ev)
	}
}

