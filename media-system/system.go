package main

import (
	//"time"
	"context"
	
	"bitbucket.org/dkolbly/logging"
)

var log = logging.New("sys")

type System struct {
	current MajorMode
	sleep *SleepMode
	clock *ClockMode
}

type MajorMode interface {
	Start(context.Context)
	Finish(context.Context)
}

func (sys *System) enter(ctx context.Context, m MajorMode) {
	if sys.current != nil {
		sys.current.Finish(ctx)
	}
	log.Debugf(ctx, "ENTERING %T", m)
	m.Start(ctx)
	sys.current = m
}


/*
shutdown is not an option

	time.Sleep(10*time.Second)
	
	log.Debugf(ctx, "FINISHING %T", sys.current)
	sys.current.Finish(ctx)
*/

func main() {

	logging.SetHumanOutput(false, false, "DEBUG")
	d := NewDisplayClient()
	defer d.Disable()
	
	sys := &System{
		sleep: &SleepMode{d},
		clock: NewClockMode(d),
	}

	// start
	sys.enter(context.Background(), sys.clock)
	// main loop
	sys.watch(context.Background(), d)
}
