package main

import (
	"bitbucket.org/dkolbly/gokrazy/lib/display"
)

type DisplayClient struct {
	*display.DisplayClient
}


func NewDisplayClient() *DisplayClient {
	return &DisplayClient{
		display.NewDisplayClient(),
	}
}
