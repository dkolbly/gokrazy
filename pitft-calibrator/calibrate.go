package main

import (
	"context"
	"fmt"
	"image"
	"io/ioutil"
	"os"
	"time"

	"bitbucket.org/dkolbly/gokrazy/lib/display"
	"bitbucket.org/dkolbly/logging"
	"github.com/dkolbly/cli"
)

var log = logging.New("cal")

func main() {

	logging.SetHumanOutput(false, false, "DEBUG")

	app := &cli.App{
		Name:  "pitft-calibrate",
		Usage: "run the calibration protocol",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "server",
				Usage: "display server",
				Value: "http://pi3:8002",
			},
		},
		Action: doCalibrate,
	}

	app.Run(os.Args)
}

func doCalibrate(c *cli.Context) error {
	d := display.NewDisplayClient(c.String("server"))
	ctx := context.Background()
	d.Disable(ctx)

	unit := [6]float64{1, 0, 0, 1, 0, 0}
	d.TouchCalibration(unit)

	var cal [4]image.Point
	for i := 1; i <= 4; i++ {
		time.Sleep(500 * time.Millisecond)
		d.PaintPNG(calImage(i), image.ZP)
		d.Flush()
		d.Enable(ctx)

		ev, err := d.GetClick(ctx)
		if err != nil {
			log.Debugf(ctx, "fail %s", err)
			return err
		}
		log.Debugf(ctx, "Point[%d] --> %#v", i, ev)
		d.Disable(ctx)
		cal[i-1].X = ev.X
		cal[i-1].Y = ev.Y
	}

	displayArea := d.Size()

	log.Debugf(ctx, "Lower Left  cal[0]   %4d %4d", cal[0].X, cal[0].Y)
	log.Debugf(ctx, "Upper Left  cal[1]   %4d %4d", cal[1].X, cal[1].Y)
	log.Debugf(ctx, "Upper Right cal[2]   %4d %4d", cal[2].X, cal[2].Y)
	log.Debugf(ctx, "Lower Right cal[3]   %4d %4d", cal[3].X, cal[3].Y)

	//      [1]   [2]
	//
	//      [0]   [3]
	//

	mat.NewDense(
	// see if we are reflected (the coordinate system is reflected if
	// the Y component varies across [1]--[2] than the X component
	// does)
	reflected := abs(cal[2].Y-cal[1].Y) > abs(cal[2].X-cal[1].X)

	if reflected {
		log.Debugf(ctx, "(reflected)")
		for i := 0; i < 4; i++ {
			cal[i].X, cal[i].Y = cal[i].Y, cal[i].X
		}
	}
	dx1 := cal[2].X - cal[1].X
	dx2 := cal[3].X - cal[0].X

	log.Debugf(ctx, "dx  top: %d   bottom: %d", dx1, dx2)

	dx := (dx1 + dx2) / 2
	xscale := float64(dx) / float64(displayArea.X-32*2)
	log.Debugf(ctx, "X scale is %.3f", xscale)

	dy1 := cal[2].X - cal[1].X
	dy2 := cal[3].X - cal[0].X

	log.Debugf(ctx, "dx  top: %d   bottom: %d", dx1, dx2)

	dx := (dx1 + dx2) / 2
	xscale := float64(dx) / float64(displayArea.X-32*2)
	log.Debugf(ctx, "X scale is %.3f", xscale)
	
	return nil
}

func calImage(k int) []byte {
	// TODO: inline assets
	img, err := ioutil.ReadFile(fmt.Sprintf("cal%d.png", k))
	if err != nil {
		panic(err)
	}
	return img
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
